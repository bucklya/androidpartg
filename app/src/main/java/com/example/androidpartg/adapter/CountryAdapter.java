package com.example.androidpartg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidpartg.databinding.ItemCountryBinding;
import com.example.androidpartg.network.model.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {

    private Context context;
    private List<Country> original = new ArrayList<>();
    private List<Country> filtered = new ArrayList<>();

    public CountryAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCountryBinding binding = ItemCountryBinding.inflate(LayoutInflater.from(context), parent, false);
        return new CountryHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
        Country country = filtered.get(position);
        holder.binding.name.setText(country.name);
        holder.binding.population.setText(Integer.toString(country.population));
        holder.binding.area.setText(Double.toString(country.area));
        holder.binding.region.setText(country.region);
    }

    @Override
    public int getItemCount() {
        return filtered.size();
    }

    public void update(List<Country> countries) {
        original.clear();
        original.addAll(countries);
        filtered.clear();
        filtered.addAll(countries);
        notifyDataSetChanged();
    }

    public void filterPopulation(int population) {
        filtered.clear();
        for (Country country : original) {
            if (country.population > population * 0.85 && country.population < population * 1.15) {
                filtered.add(country);
            }
        }
        notifyDataSetChanged();
        if (filtered.isEmpty()) {
            Toast.makeText(context,"Стран с таким населением не существует", Toast.LENGTH_SHORT).show();
        }
    }

    public void filterArea(int area) {
        filtered.clear();
        for (Country country : original) {
            if (country.area > area * 0.85 && country.area < area * 1.15) {
                filtered.add(country);
            }
        }
        notifyDataSetChanged();
        if (filtered.isEmpty()) {
            Toast.makeText(context,"Стран с такой площадью не существует", Toast.LENGTH_SHORT).show();
        }
    }

    public void filterRegion (String region) {
        filtered.clear();
        for (Country country : original) {
            if (country.region.toLowerCase().contains(region.toLowerCase())) {
                filtered.add(country);
            }
        }
        notifyDataSetChanged();
        if (filtered.isEmpty()) {
            Toast.makeText(context,"Такого региона не существует", Toast.LENGTH_SHORT).show();
        }
    }

    public void showAll() {
        filtered.clear();
        filtered.addAll(original);
        notifyDataSetChanged();
    }

    static class CountryHolder extends RecyclerView.ViewHolder {

        public ItemCountryBinding binding;

        public CountryHolder(ItemCountryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
