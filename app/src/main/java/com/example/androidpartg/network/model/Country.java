package com.example.androidpartg.network.model;

public class Country {
    public String name;
    public double area;
    public int population;
    public String region;
}
