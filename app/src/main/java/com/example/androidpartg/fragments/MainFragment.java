package com.example.androidpartg.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.androidpartg.R;
import com.example.androidpartg.adapter.CountryAdapter;
import com.example.androidpartg.databinding.FragmentMainBinding;
import com.example.androidpartg.network.ApiService;
import com.example.androidpartg.network.model.Country;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainFragment extends Fragment {

    private FragmentMainBinding binding;
    private CountryAdapter adapter;
    private Disposable disposable;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater, container, false);
        View v = binding.getRoot();

        adapter = new CountryAdapter(v.getContext());
        binding.listCountry.setAdapter(adapter);

        disposable = ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showListCountries, this::showError);

        binding.searchPopulation.setOnClickListener(v1 -> {
            if (!binding.inputPopulation.getText().toString().isEmpty()) {
                adapter.filterPopulation(Integer.parseInt(binding.inputPopulation.getText().toString()));
            } else {
                Toast.makeText(binding.getRoot().getContext(), "Поле ввода пустое", Toast.LENGTH_SHORT).show();
            }
        });

        binding.searchArea.setOnClickListener(v12 -> {
            if (!binding.inputArea.getText().toString().isEmpty()) {
                adapter.filterArea(Integer.parseInt(binding.inputArea.getText().toString()));
            } else {
                Toast.makeText(binding.getRoot().getContext(), "Поле ввода пустое", Toast.LENGTH_SHORT).show();
            }
        });

        binding.searchRegion.setOnClickListener(v13 -> {
            if (!binding.inputRegion.getText().toString().isEmpty()) {
                adapter.filterRegion(binding.inputRegion.getText().toString());
            } else {
                Toast.makeText(binding.getRoot().getContext(), "Поле ввода пустое", Toast.LENGTH_SHORT).show();
            }
        });

        binding.allList.setOnClickListener(v14 -> adapter.showAll());

        binding.showSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.showSettings.setVisibility(View.GONE);
                binding.fieldSettings.setVisibility(View.VISIBLE);
            }
        });

        binding.hideSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.fieldSettings.setVisibility(View.GONE);
                binding.showSettings.setVisibility(View.VISIBLE);
            }
        });

        return v;
    }

    public void showListCountries(List<Country> countries) {
        adapter.update(countries);
    }

    public void showError(Throwable t) {
        Toast.makeText(binding.getRoot().getContext(), "ERROR", Toast.LENGTH_SHORT).show();
    }
}
